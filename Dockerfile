FROM node:12.13-alpine As api

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

ADD . /usr/src/app

RUN npm build

EXPOSE 3000

CMD node dist/main.js
