import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
  Query
} from "@nestjs/common";
import { CarsService } from "./cars.service";
import { CreateCar } from "./dto/create-car.dto";
import { CarDocument } from "./schemas/car.schema";

@Controller("cars")
export class CarsController {
  constructor(private carService: CarsService) {}

  @Get()
  findAll(@Query("pick") pick: string | string[]): Promise<CarDocument[]> {
    return this.carService.findAll(pick);
  }
  @Get(":id")
  async getById(
    @Param("id") id: string,
    @Query("pick") pick: string | string[]
  ): Promise<CarDocument> {
    const car = await this.carService.findById(id, pick);
    if (!car) throw new NotFoundException();
    return car;
  }
  @Post()
  addOne(@Body() car: CreateCar): Promise<CarDocument> {
    return this.carService.createOne(car);
  }
  @Delete(":id")
  async deleteOne(@Param("id") id: string): Promise<void> {
    const result = await this.carService.deleteOne(id);
    if (!result) throw new NotFoundException();
  }
  @Put(":id")
  async update(
    @Param("id") id: string,
    @Body() car: CarDocument
  ): Promise<void> {
    const updated = await this.carService.updateOne(id, car);
    if (!updated) throw new NotFoundException();
  }
}
