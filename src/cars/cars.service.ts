import { Injectable } from "@nestjs/common";
import { Car, CarDocument } from "./schemas/car.schema";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Cron } from "@nestjs/schedule";
import * as moment from "moment";

@Injectable()
export class CarsService {
  constructor(@InjectModel(Car.name) private carModel: Model<CarDocument>) {}

  findAll(pick?: string | string[]): Promise<CarDocument[]> {
    return this.findQueryCreator({ pick });
  }
  async findById(id: string, pick?: string | string[]) {
    const result = await this.findQueryCreator({ id, pick });
    return result[0];
  }

  createOne(car) {
    return this.carModel.create(car);
  }
  async deleteOne(id) {
    const deleted = await this.carModel.deleteOne({ _id: id });
    return deleted && deleted.deletedCount;
  }
  async updateOne(id, car: CarDocument) {
    const updated = await this.carModel.updateOne({ _id: id }, car);
    return updated && updated.nModified;
  }

  private findQueryCreator(FindQuery: {
    id?: string;
    pick?: string | string[];
  }) {
    const filter: { [key: string]: any } = {};
    const select: string[] = [];

    const { id, pick } = FindQuery;

    if (pick) {
      if (typeof pick === "string") {
        select.push(pick);
      } else {
        select.push(...pick);
      }
    }

    if (id) {
      filter._id = id;
    }
    const query = this.carModel.find(filter);
    if (select.length) {
      if (!select.includes("_id")) select.push("-_id");
      query.select(select.join(" "));
    }

    return query.exec();
  }

  @Cron("*/1 * * * *")
  private async handleCron() {
    await Promise.all([
      this.carModel.updateMany(
        {
          firstRegistrationDate: {
            $lte: moment()
              .subtract({ month: 12 })
              .toDate(),
            $gte: moment()
              .subtract({ month: 18 })
              .toDate()
          }
        },
        { $mul: { price: 0.8 } }
      ),
      this.carModel.update(
        {},
        {
          $pull: {
            owners: {
              // @ts-ignore
              $elemMatch: {
                firstRegistrationDate: {
                  $lte: moment()
                    .subtract({ month: 18 })
                    .toDate()
                }
              }
            }
          }
        },
        { multi: true }
      )
    ]);
  }
}
