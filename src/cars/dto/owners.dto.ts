import { IsDateString, IsString } from "class-validator";

export class Owners {
  @IsString()
  name: string;

  @IsDateString()
  firstRegistrationDate: Date;
}
