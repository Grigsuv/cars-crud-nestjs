import { IsPhoneNumber, IsString } from "class-validator";

export class ManufacturerDto {
  @IsString()
  name: string;

  @IsPhoneNumber(null)
  phone: string;

  @IsString()
  serial: string;
}
