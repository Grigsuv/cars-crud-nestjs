import {
  ArrayMinSize,
  IsArray,
  IsDateString,
  IsNumber,
  IsObject,
  ValidateNested
} from "class-validator";
import { ManufacturerDto } from "./manufacturer.dto";
import { Type } from "class-transformer";
import { Owners } from "./owners.dto";

export class CreateCar {
  @IsNumber()
  price: number;

  @IsDateString()
  firstRegistrationDate: Date;

  @IsObject()
  @ValidateNested()
  @Type(() => ManufacturerDto)
  manufacturer: ManufacturerDto;

  @IsArray()
  @ValidateNested({ each: true })
  @ArrayMinSize(1)
  @Type(() => Owners)
  owners: Owners[];
}
