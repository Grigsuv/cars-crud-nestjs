import { Prop, Schema } from "@nestjs/mongoose";

@Schema()
export class ManufacturerSchema {
  @Prop()
  name: string;

  @Prop()
  phone: string;

  @Prop()
  serial: number;
}
