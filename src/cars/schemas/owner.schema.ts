import { Prop, Schema } from "@nestjs/mongoose";

@Schema()
export class OwnerSchema {
  @Prop()
  name: string;

  @Prop()
  age: Date;
}
