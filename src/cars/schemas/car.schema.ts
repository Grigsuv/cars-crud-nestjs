import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import { ManufacturerSchema } from "./manufacturer.schema";
import { OwnerSchema } from "./owner.schema";

export type CarDocument = Car & Document;

@Schema()
export class Car {
  @Prop({ required: true })
  price: number;

  @Prop({ default: new Date() })
  firstRegistrationDate: Date;

  @Prop({ required: true })
  manufacturer: ManufacturerSchema;

  @Prop({ required: true })
  owners: OwnerSchema[];
}

export const CarSchema = SchemaFactory.createForClass(Car);
