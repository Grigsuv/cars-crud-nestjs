import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { CarsModule } from "./cars/cars.module";
import { MongooseModule } from "@nestjs/mongoose";
import { ScheduleModule } from "@nestjs/schedule";

@Module({
  imports: [
    ScheduleModule.forRoot(),
    MongooseModule.forRoot("mongodb://localhost/car-app", {
      useNewUrlParser: true
    }),
    CarsModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
